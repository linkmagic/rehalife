class Constants {

    static DefaultValues = {
        uiLangId: 'EN'
    };

    static LocalStorageKey = {
        userTokenData: 'USER_TOKEN_DATA',
        expiresDate: 'EXPIRES_DATE',
        uiLang: 'UI_LANG'
    };

    static ReducerEvent = {
        displayPopup: 'DISPLAY_POPUP',
        displayPanel: 'DISPLAY_PANEL',
        displayContent: 'DISPLAY_CONTENT',
        displayContentCabinet: 'DISPLAY_CONTENT_CABINET',
        uiLangDownload: 'UI_LANG_DOWNLOAD',
        uiLangChange: 'UI_LANG_CHANGE',
        uiLang: 'UI_LANG',
        otherData: 'OTHER_DATA',
        messageBoxUpdate: 'MESSAGE_BOX_UPDATE',
        messageBoxAdd: 'MESSAGE_BOX_ADD',
        trainingLetters: 'TRAINING_LETTERS'
    };

    static DisplayContent = {
        home: 'HOME',
        help: 'HELP',
        login: 'LOGIN',
        mainMenuTrainings: 'MAIN_MENU_TRAININGS',
        mainMenuScreening: 'MAIN_MENU_SCREENING',
        trainingTopologicalMemory: 'TRAINING_TOPOLOGICAL_MEMORY'
    };

}

export default Constants;

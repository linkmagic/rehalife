class DOMProcess {

    static findClassInElem(domElem, className) {
        let classList = domElem.className.split(' ');
        return (classList.indexOf(className) >= 0);
    }

    static removeClassFromElem(domElem, className) {
        let classList = domElem.className.split(' ');
        let classListNew = '';
        for (let classItem in classList) {
            if (classList[classItem] === className) {
                continue;
            }
            classListNew += classList[classItem] + ' ';
        }
        domElem.className = classListNew;
    }

    static addClassToElem(domElem, className) {
        domElem.className = domElem.className + ' ' + className
    }

    static toggleClassElem(domElem, className) {
        let classList = domElem.className.split(' ');
        for (let i = 0; i < classList.length; i++) {
            if (classList[i] === className) {
                classList.splice(i, 1);
                domElem.className = classList.join(' ');
                return;
            }
        }
        classList.push(className);
        domElem.className = classList.join(' ');
    }

}

export default DOMProcess;

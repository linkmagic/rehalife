class LocalStorage {

    static find(key) {
        return !!(localStorage.getItem(key));
    }

    static remove(key) {
        if (this.find(key)) {
            localStorage.removeItem(key);
        }
    }

    static set(key, dataObject) {
        try {
            localStorage.setItem(key, JSON.stringify(dataObject));
        } catch (exception) {
            console.log(exception);
        }
    }

    static setStr(key, dataStr) {
        try {
            localStorage.setItem(key, dataStr);
        } catch (exception) {
            console.log(exception);
        }
    }

    static get(key) {
        let dataObject = localStorage.getItem(key);
        if (dataObject) {
            return JSON.parse(dataObject);
        } else {
            return null;
        }
    }

    static getStr(key) {
        let dataStr = localStorage.getItem(key);
        if (dataStr) {
            return dataStr;
        } else {
            return null;
        }
    }

}

export default LocalStorage;

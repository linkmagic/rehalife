import Constants from '../Constants';

const initialState = {
    trainingLevel: 0,
    lettersTrainingArray: [],
    lettersTrainingCount: 0,
    cardLetterCollection: [],
    letterTask: '',
    errorsMax: 0,
    errorsCount: 0,
    trainigIsEndByError: false,
    trainigIsEndBySuccess: false,
    timeStart: null,
    timeEnd: null
};

function trainingLetters(state = initialState, action) {
    if (action.type === Constants.ReducerEvent.trainingLetters) {
        let newState = {
            trainingLevel: action.action.trainingLevel,
            lettersTrainingArray: action.action.lettersTrainingArray,
            lettersTrainingCount: action.action.lettersTrainingCount,
            cardLetterCollection: action.action.cardLetterCollection,
            cardLetterPropsCollection: action.action.cardLetterPropsCollection,
            letterTask: action.action.letterTask,
            errorsMax: action.action.errorsMax,
            errorsCount: action.action.errorsCount,
            trainigIsEndByError: action.action.trainigIsEndByError,
            trainigIsEndBySuccess: action.action.trainigIsEndBySuccess,
            timeStart: action.action.timeStart,
            timeEnd: action.action.timeEnd
        };
        return newState;
    }
    return state;
}

export default trainingLetters;

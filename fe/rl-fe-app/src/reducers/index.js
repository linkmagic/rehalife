import { combineReducers } from 'redux';

import displayContent from './displayContent';
import otherData from './otherData';
import uiLang from './uiLang';
import trainingLetters from './trainingLetters';

export default combineReducers({
    displayContent,
    otherData,
    uiLang,
    trainingLetters
});

import Constants from '../Constants';
import LocalStorage from '../utils/LocalStorage'

const initialState = {
    list: [],
    current: LocalStorage.getStr(Constants.LocalStorageKey.uiLang) ? LocalStorage.getStr(Constants.LocalStorageKey.uiLang) : Constants.DefaultValues.uiLangId
};

function uiLang(state = initialState, action) {
    if (action.type === Constants.ReducerEvent.uiLangDownload) {
        return {
            ...state,
            list: action.action
        };
    }
    if (action.type === Constants.ReducerEvent.uiLangChange) {
        return {
            ...state,
            current: action.action
        };
    }
    return state;
}

export default uiLang;

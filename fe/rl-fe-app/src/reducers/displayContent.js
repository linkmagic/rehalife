import Constants from '../Constants';

const initialState = {
    // name: Constants.DisplayContent.login
    name: Constants.DisplayContent.trainingTopologicalMemory
};

export default function displayContent(state = initialState, action) {

    if (action.type === Constants.ReducerEvent.displayContent) {
        return {
            ...state,
            name: action.action
        };
    }

    return state;
}
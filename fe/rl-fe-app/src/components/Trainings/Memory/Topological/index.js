import React, { Component } from 'react';
import { connect } from 'react-redux';

import './style.css';

import Constants from '../../../../Constants';
import FlipCard from '../../Components/FlipCard';

class Topological extends Component {

    letters = [
        'А', 'Б', 'В', 'Г', 'Ґ', 'Д', 'Е',
        'Є', 'Ж', 'З', 'И', 'І', 'Ї', 'Й',
        'К', 'Л', 'М', 'Н', 'О', 'П', 'Р',
        'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч',
        'Ш', 'Щ', 'Ь', 'Ю', 'Я'
    ];

    cardClick = (e) => {
        console.log(e.target);
    };

    trainingStart = () => {
        console.log('TRAINING STARTED', Date.now());

        // level value 1..10
        const trainingLevel = 2;

        let newState = {
            trainingLevel: trainingLevel,
            lettersTrainingArray: [],
            lettersTrainingCount: Math.trunc(((this.letters.length / 100) * trainingLevel) * 10),
            cardLetterCollection: [],
            letterTask: '',
            errorsMax: trainingLevel,
            errorsCount: 0,
            trainigIsEndByError: false,
            trainigIsEndBySuccess: false,
            timeStart: null,
            timeEnd: null
        };

        let indexLetter, letterExists;

        // filling an array of training
        for (let i = 0; i < newState.lettersTrainingCount; ) {
            indexLetter = Math.floor(Math.random() * this.letters.length);
            letterExists = false;
            for (let c = 0; c < newState.lettersTrainingArray.length; c++) {
                if (newState.lettersTrainingArray[c] === this.letters[indexLetter]) {
                    letterExists = true;
                    break;
                }
            }
            if (!letterExists) {
                newState.lettersTrainingArray.push(this.letters[indexLetter]);
                i++;
            }
        }

        // filling an array ui cards components
        for (let i = 0; i < newState.lettersTrainingCount; i++) {
            newState.cardLetterCollection.push(
                <FlipCard key={i}
                          letter={newState.lettersTrainingArray[i]}
                          backSymbol={"%"}
                          flipperStyle={{transform: 'rotateY(0deg)'}} />
            );
        }

        // this.setState(newState);
        this.props.onTrainingLettersChange(newState);

        setTimeout(() => this.turnCardsFace(), 500);
        setTimeout(() => this.turnCardsBack(), 2500);
        setTimeout(() => this.generateLetterTask(), 3500);
    };

    trainingLoop = () => {
        // for (let i = 0; i < parseInt(this.trainingsCountInput.value, 10); i++) {
        //     this.trainingStart();
        // }
    };

    turnCardsFace = () => {
        let { trainingLetters } = this.props.AppState;

        for (let i = 0; i < trainingLetters.cardLetterCollection.length; i++) {
            trainingLetters.cardLetterCollection[i] = <FlipCard key={trainingLetters.cardLetterCollection[i].key}
                                                letter={trainingLetters.cardLetterCollection[i].props.letter}
                                                backSymbol={trainingLetters.cardLetterCollection[i].props.backSymbol}
                                                flipperStyle={{ transform: 'rotateY(180deg)' }} />
        }

        this.props.onTrainingLettersChange(trainingLetters);
    };

    turnCardsBack = () => {
       let { trainingLetters } = this.props.AppState;

        for (let i = 0; i < trainingLetters.cardLetterCollection.length; i++) {
            trainingLetters.cardLetterCollection[i] = <FlipCard key={trainingLetters.cardLetterCollection[i].key}
                                                letter={trainingLetters.cardLetterCollection[i].props.letter}
                                                backSymbol={trainingLetters.cardLetterCollection[i].props.backSymbol}
                                                flipperStyle={{transform: 'rotateY(0deg)'}} />
        }

        this.props.onTrainingLettersChange(trainingLetters);
    };

    generateLetterTask = () => {
       let { trainingLetters } = this.props.AppState;

       trainingLetters.letterTask = trainingLetters.lettersTrainingArray[Math.trunc(Math.random() * trainingLetters.lettersTrainingArray.length)];
       trainingLetters.timeStart = Date.now();
       this.props.onTrainingLettersChange(trainingLetters);
    };

    trainingStop = () => {
        // if (this.timerTrainingItem) {
        //     clearInterval(this.timerTrainingItem);
        //     this.timerTrainingItem = null;
        // }
    };

    render() {
        let { trainingLetters } = this.props.AppState;

        return (
            <div className="Training__Memory__Topological">
                <div className="Topological__Sidebar">

                    <div className="Training__IndicatorLamps">
                        <div className="cont_lamps">
                            <div id="idLampError" className="lamp_error"></div>
                            <div id="idLampOk" className="lamp_ok"></div>
                        </div>
                    </div>


                    <div className="Training__ActionResult">
                        <div class="Training__ActionResult__Value">
                            timeDiff
                        </div>
                    </div>

                    <div className="Training__ActionTask">
                        <div className="Training__ActionTask__Value">
                            <span>{trainingLetters.letterTask}</span>
                        </div>
                    </div>

                    <div className="Training__Details">

                        <table>
                            <tr>
                                <td>Уровень</td>
                                <td><input type="number" min="1" max="10" class="Training__Details__TrainingsCountInput" /></td>
                            </tr>
                            <tr>
                                <td>Макс. ошибок</td>
                                <td><input type="number" min="1" max="5" class="Training__Details__TrainingsCountInput" /></td>
                            </tr>
                            <tr>
                                <td>Время (сек)</td>
                                <td><input type="number" min="1" max="60" class="Training__Details__TrainingsCountInput" /></td>
                            </tr>
                        </table>

                        <div class="Training__Details">
                            <div className="ctrlButton ctrlBtn_start" onClick={this.trainingStart}/>
                            <div class="ctrlButton ctrlBtn_stop" onClick={this.trainingStop}/>
                        </div>

                        <input className="Training__Details__TrainingsCountInput" ref={(c) => this.trainingsCountInput = c} defaultValue="3"/>

                    </div>
                </div>

                <div className="Topological__WorkingPlace">
                    {
                        trainingLetters.trainigIsEndBySuccess
                            ? <h3>Training success! Time: {((trainingLetters.timeEnd - trainingLetters.timeStart) / 1000).toFixed(2)} s</h3>
                            : ''
                    }
                    {
                        trainingLetters.trainigIsEndByError
                            ? <h3>Maximum number of errors. Training finished!</h3>
                            : ''
                    }
                    {
                        trainingLetters.cardLetterCollection
                            ? trainingLetters.cardLetterCollection.map((item)=> {
                                return item;
                            })
                            : ''
                    }
                </div>

            </div>
        );
    }

}

export default connect(

    state => ({
        AppState: state
    }),

    dispatch => ({

        onDisplayContentChange: (action) => {
            dispatch({ type: Constants.ReducerEvent.displayContent, action });
        },

        onTrainingLettersChange: (action) => {
            dispatch({ type: Constants.ReducerEvent.trainingLetters, action })
        }

    })

)(Topological);

import React, { Component } from 'react';
import { connect } from 'react-redux';

import './style.css';
import Constants from '../../../../Constants';

class FlipCard extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isTurned: false
        };
    }

    flipperClick = () => {
        let { trainingLetters } = this.props.AppState;

        if (this.props.letter === trainingLetters.letterTask) {
            trainingLetters.trainigIsEndBySuccess = true;
            trainingLetters.timeEnd = Date.now();
            this.props.onTrainingLettersChange(trainingLetters);
        } else {
            trainingLetters.errorsCount++;
            if (trainingLetters.errorsCount === trainingLetters.errorsMax) {
                trainingLetters.trainigIsEndByError = true;
                trainingLetters.timeEnd = Date.now();
                this.props.onTrainingLettersChange(trainingLetters);
            }
        }
        
        this.setState({
            isTurned: !this.state.isTurned
        });
        
        this.flipperCard.style = this.state.isTurned ? 'transform: rotateY(0deg)' : 'transform: rotateY(180deg)';
    };

    render() {
        const {
            letter,
            backSymbol,
            flipperStyle
        } = this.props;

        return (
            <div className="flip-container">

                <div className="flipper" 
                     ref={(c) => this.flipperCard = c}
                     onClick={this.flipperClick}
                     style={flipperStyle}>

                    <div className="front" ref={(c) => this.flipperCardFront = c}>
                        {backSymbol}
                    </div>
                    
                    <div className="back" ref={(c) => this.flipperCardBack = c}>
                        {letter}
                    </div>

                </div>
                
            </div>
        );
    }

}

export default connect(

    state => ({
        AppState: state
    }),

    dispatch => ({

        onTrainingLettersChange: (action) => {
            dispatch({ type: Constants.ReducerEvent.trainingLetters, action })
        }

    })

)(FlipCard);

import React, { Component } from 'react';
import { connect } from 'react-redux';

import './style.css';

import Constants from '../../../Constants';
import LocalStorage from '../../../utils/LocalStorage';
import DOMProcess from '../../../utils/DOMProcess';

class LangSwitcher extends Component {

    langBtnCurrClick = () => {
        DOMProcess.toggleClassElem(this.divLCPList, 'LangListMenu-show');
        DOMProcess.toggleClassElem(this.divLangControlPanel, 'LanguageControlPanel-active');
    };

    uiLangENCick = () => {
        this.uiLangApply('EN');
    };

    uiLangUKCick = () => {
        this.uiLangApply('UK');
    };

    uiLangRUCick = () => {
        this.uiLangApply('RU');
    };

    uiLangApply = (langId) => {
        LocalStorage.setStr(Constants.ReducerEvent.uiLang, langId);
        this.props.onUiLangChange(langId);
        DOMProcess.removeClassFromElem(this.divLCPList, 'LangListMenu-show');
        DOMProcess.toggleClassElem(this.divLangControlPanel, 'LanguageControlPanel-active');
    };

    getUiLangFlag = (uiLangId) => {
        
        switch (uiLangId) {
            
            case 'EN': {
                return (
                    <svg xmlns="http://www.w3.org/2000/svg" id="flag-icon-css-us" viewBox="80 0 480 480" className="svgFlagUiLang">
                        <g fillRule="evenodd">
                            <g strokeWidth="1pt">
                                <path fill="#bd3d44" d="M0 0h972.8v39.4H0zm0 78.8h972.8v39.4H0zm0 78.7h972.8V197H0zm0 78.8h972.8v39.4H0zm0 78.8h972.8v39.4H0zm0 78.7h972.8v39.4H0zm0 78.8h972.8V512H0z" transform="scale(.9375)"/>
                                <path fill="#fff" d="M0 39.4h972.8v39.4H0zm0 78.8h972.8v39.3H0zm0 78.7h972.8v39.4H0zm0 78.8h972.8v39.4H0zm0 78.8h972.8v39.4H0zm0 78.7h972.8v39.4H0z" transform="scale(.9375)"/>
                            </g>
                            <path fill="#192f5d" d="M0 0h389.1v275.7H0z" transform="scale(.9375)"/>
                            <path fill="#fff" d="M32.4 11.8L36 22.7h11.4l-9.2 6.7 3.5 11-9.3-6.8-9.2 6.7 3.5-10.9-9.3-6.7H29zm64.9 0l3.5 10.9h11.5l-9.3 6.7 3.5 11-9.2-6.8-9.3 6.7 3.5-10.9-9.2-6.7h11.4zm64.8 0l3.6 10.9H177l-9.2 6.7 3.5 11-9.3-6.8-9.2 6.7 3.5-10.9-9.3-6.7h11.5zm64.9 0l3.5 10.9H242l-9.3 6.7 3.6 11-9.3-6.8-9.3 6.7 3.6-10.9-9.3-6.7h11.4zm64.8 0l3.6 10.9h11.4l-9.2 6.7 3.5 11-9.3-6.8-9.2 6.7 3.5-10.9-9.2-6.7h11.4zm64.9 0l3.5 10.9h11.5l-9.3 6.7 3.6 11-9.3-6.8-9.3 6.7 3.6-10.9-9.3-6.7h11.5zM64.9 39.4l3.5 10.9h11.5L70.6 57 74 67.9 65 61.2l-9.3 6.7L59 57 50 50.3h11.4zm64.8 0l3.6 10.9h11.4l-9.3 6.7 3.6 10.9-9.3-6.7-9.3 6.7L124 57l-9.3-6.7h11.5zm64.9 0l3.5 10.9h11.5l-9.3 6.7 3.5 10.9-9.2-6.7-9.3 6.7 3.5-10.9-9.2-6.7H191zm64.8 0l3.6 10.9h11.4l-9.3 6.7 3.6 10.9-9.3-6.7-9.2 6.7 3.5-10.9-9.3-6.7H256zm64.9 0l3.5 10.9h11.5L330 57l3.5 10.9-9.2-6.7-9.3 6.7 3.5-10.9-9.2-6.7h11.4zM32.4 66.9L36 78h11.4l-9.2 6.7 3.5 10.9-9.3-6.8-9.2 6.8 3.5-11-9.3-6.7H29zm64.9 0l3.5 11h11.5l-9.3 6.7 3.5 10.9-9.2-6.8-9.3 6.8 3.5-11-9.2-6.7h11.4zm64.8 0l3.6 11H177l-9.2 6.7 3.5 10.9-9.3-6.8-9.2 6.8 3.5-11-9.3-6.7h11.5zm64.9 0l3.5 11H242l-9.3 6.7 3.6 10.9-9.3-6.8-9.3 6.8 3.6-11-9.3-6.7h11.4zm64.8 0l3.6 11h11.4l-9.2 6.7 3.5 10.9-9.3-6.8-9.2 6.8 3.5-11-9.2-6.7h11.4zm64.9 0l3.5 11h11.5l-9.3 6.7 3.6 10.9-9.3-6.8-9.3 6.8 3.6-11-9.3-6.7h11.5zM64.9 94.5l3.5 10.9h11.5l-9.3 6.7 3.5 11-9.2-6.8-9.3 6.7 3.5-10.9-9.2-6.7h11.4zm64.8 0l3.6 10.9h11.4l-9.3 6.7 3.6 11-9.3-6.8-9.3 6.7 3.6-10.9-9.3-6.7h11.5zm64.9 0l3.5 10.9h11.5l-9.3 6.7 3.5 11-9.2-6.8-9.3 6.7 3.5-10.9-9.2-6.7H191zm64.8 0l3.6 10.9h11.4l-9.2 6.7 3.5 11-9.3-6.8-9.2 6.7 3.5-10.9-9.3-6.7H256zm64.9 0l3.5 10.9h11.5l-9.3 6.7 3.5 11-9.2-6.8-9.3 6.7 3.5-10.9-9.2-6.7h11.4zM32.4 122.1L36 133h11.4l-9.2 6.7 3.5 11-9.3-6.8-9.2 6.7 3.5-10.9-9.3-6.7H29zm64.9 0l3.5 10.9h11.5l-9.3 6.7 3.5 10.9-9.2-6.7-9.3 6.7 3.5-10.9-9.2-6.7h11.4zm64.8 0l3.6 10.9H177l-9.2 6.7 3.5 11-9.3-6.8-9.2 6.7 3.5-10.9-9.3-6.7h11.5zm64.9 0l3.5 10.9H242l-9.3 6.7 3.6 11-9.3-6.8-9.3 6.7 3.6-10.9-9.3-6.7h11.4zm64.8 0l3.6 10.9h11.4l-9.2 6.7 3.5 11-9.3-6.8-9.2 6.7 3.5-10.9-9.2-6.7h11.4zm64.9 0l3.5 10.9h11.5l-9.3 6.7 3.6 11-9.3-6.8-9.3 6.7 3.6-10.9-9.3-6.7h11.5zM64.9 149.7l3.5 10.9h11.5l-9.3 6.7 3.5 10.9-9.2-6.8-9.3 6.8 3.5-11-9.2-6.7h11.4zm64.8 0l3.6 10.9h11.4l-9.3 6.7 3.6 10.9-9.3-6.8-9.3 6.8 3.6-11-9.3-6.7h11.5zm64.9 0l3.5 10.9h11.5l-9.3 6.7 3.5 10.9-9.2-6.8-9.3 6.8 3.5-11-9.2-6.7H191zm64.8 0l3.6 10.9h11.4l-9.2 6.7 3.5 10.9-9.3-6.8-9.2 6.8 3.5-11-9.3-6.7H256zm64.9 0l3.5 10.9h11.5l-9.3 6.7 3.5 10.9-9.2-6.8-9.3 6.8 3.5-11-9.2-6.7h11.4zM32.4 177.2l3.6 11h11.4l-9.2 6.7 3.5 10.8-9.3-6.7-9.2 6.7 3.5-10.9-9.3-6.7H29zm64.9 0l3.5 11h11.5l-9.3 6.7 3.6 10.8-9.3-6.7-9.3 6.7 3.6-10.9-9.3-6.7h11.4zm64.8 0l3.6 11H177l-9.2 6.7 3.5 10.8-9.3-6.7-9.2 6.7 3.5-10.9-9.3-6.7h11.5zm64.9 0l3.5 11H242l-9.3 6.7 3.6 10.8-9.3-6.7-9.3 6.7 3.6-10.9-9.3-6.7h11.4zm64.8 0l3.6 11h11.4l-9.2 6.7 3.5 10.8-9.3-6.7-9.2 6.7 3.5-10.9-9.2-6.7h11.4zm64.9 0l3.5 11h11.5l-9.3 6.7 3.6 10.8-9.3-6.7-9.3 6.7 3.6-10.9-9.3-6.7h11.5zM64.9 204.8l3.5 10.9h11.5l-9.3 6.7 3.5 11-9.2-6.8-9.3 6.7 3.5-10.9-9.2-6.7h11.4zm64.8 0l3.6 10.9h11.4l-9.3 6.7 3.6 11-9.3-6.8-9.3 6.7 3.6-10.9-9.3-6.7h11.5zm64.9 0l3.5 10.9h11.5l-9.3 6.7 3.5 11-9.2-6.8-9.3 6.7 3.5-10.9-9.2-6.7H191zm64.8 0l3.6 10.9h11.4l-9.2 6.7 3.5 11-9.3-6.8-9.2 6.7 3.5-10.9-9.3-6.7H256zm64.9 0l3.5 10.9h11.5l-9.3 6.7 3.5 11-9.2-6.8-9.3 6.7 3.5-10.9-9.2-6.7h11.4zM32.4 232.4l3.6 10.9h11.4l-9.2 6.7 3.5 10.9-9.3-6.7-9.2 6.7 3.5-11-9.3-6.7H29zm64.9 0l3.5 10.9h11.5L103 250l3.6 10.9-9.3-6.7-9.3 6.7 3.6-11-9.3-6.7h11.4zm64.8 0l3.6 10.9H177L168 250l3.5 10.9-9.3-6.7-9.2 6.7 3.5-11-9.3-6.7h11.5zm64.9 0l3.5 10.9H242l-9.3 6.7 3.6 10.9-9.3-6.7-9.3 6.7 3.6-11-9.3-6.7h11.4zm64.8 0l3.6 10.9h11.4l-9.2 6.7 3.5 10.9-9.3-6.7-9.2 6.7 3.5-11-9.2-6.7h11.4zm64.9 0l3.5 10.9h11.5l-9.3 6.7 3.6 10.9-9.3-6.7-9.3 6.7 3.6-11-9.3-6.7h11.5z" transform="scale(.9375)"/>
                        </g>
                    </svg>
                );
            }

            case 'UK': {
                return (
                    <svg xmlns="http://www.w3.org/2000/svg" id="flag-icon-css-ua" viewBox="80 0 480 480" className="svgFlagUiLang">
                        <g fillRule="evenodd" strokeWidth="1pt">
                            <path fill="#ffd500" d="M0 0h640v480H0z"/>
                            <path fill="#005bbb" d="M0 0h640v240H0z"/>
                        </g>
                    </svg>
                );
            }

            case 'RU': {
                return (
                    <svg xmlns="http://www.w3.org/2000/svg" id="flag-icon-css-ru" viewBox="80 0 480 480" className="svgFlagUiLang">
                        <g fillRule="evenodd" strokeWidth="1pt">
                            <path fill="#fff" d="M0 0h640v480H0z"/>
                            <path fill="#0039a6" d="M0 160h640v320H0z"/>
                            <path fill="#d52b1e" d="M0 320h640v160H0z"/>
                        </g>
                    </svg>
                );
            }

            default: return '';
        }
    };

    render() {
        let uiLangId = LocalStorage.getStr(Constants.LocalStorageKey.uiLang);

        return (
            <div className="LanguageControlPanel" ref={(c) => this.divLangControlPanel = c}>

                <div id="idUiLangBtnCurr" className="UiLangCurrBtn" onClick={this.langBtnCurrClick}>
                    {this.getUiLangFlag(uiLangId)}
                </div>

                <div className="LCP__List" ref={(c) => this.divLCPList = c}>
                
                    <div id="idUiLangBtnEn" className="UiLangBtn svgFlagTopMargin" onClick={this.uiLangENCick}>
                        {this.getUiLangFlag('EN')}
                    </div>

                    <div id="idUiLangBtnUk" className="UiLangBtn svgFlagTopMargin" onClick={this.uiLangUKCick}>
                        {this.getUiLangFlag('UK')}
                    </div>

                    <div id="idUiLangBtnRu" className="UiLangBtn svgFlagTopMargin" onClick={this.uiLangRUCick}>
                        {this.getUiLangFlag('RU')}
                    </div>

                </div>

            </div>
        );
    }

}

export default connect(

    state => ({
        AppState: state
    }),

    dispatch => ({

        onUiLangChange: (action) => {
            dispatch({ type: Constants.ReducerEvent.uiLangChange, action })
        }

    })

)(LangSwitcher);

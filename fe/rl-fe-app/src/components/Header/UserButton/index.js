import React, { Component } from 'react';
import { connect } from 'react-redux';

import '../../../';
import './style.css';
import Constants from '../../../Constants';

// import Constants from '../../../Constants';
// import LangFlags from '../../../utils/LangFlags';
// import LocalStorage from '../../../utils/LocalStorage';
// import DOMProcess from '../../../utils/DOMProcess';

class UserButton extends Component {
    
    ucpButtonClick = () => {
        // DOMProcess.toggleClassElem(this.userControlPanelMenu, 'UserControl__Menu-show');
        this.props.onDisplayContentChange(Constants.DisplayContent.login);
    };

    render() {
        return (
            <div className="UserControlPanel">
                <button className="UCP__Button" ref={(c) => this.userControlPanelButton = c} onClick={this.ucpButtonClick}>
                    <img src="http://infoling.org/infoling2/img/colorful-stickers-part-3-icons-set/png/256x256/user.png" alt="" />
                </button>
                {/*
                <div className="UserControl__Menu" ref={(c) => {this.userControlPanelMenu = c}}>
                    <button className="UC__Menu__Btn">Login</button>
                    <button className="UC__Menu__Btn">Profile</button>
                </div>
                */}
            </div>
        );
    }

}

export default connect(

    state => ({
        AppState: state
    }),

    dispatch => ({

        onDisplayContentChange: (action) => {
            dispatch({ type: Constants.ReducerEvent.displayContent, action });
        }

    })

)(UserButton);

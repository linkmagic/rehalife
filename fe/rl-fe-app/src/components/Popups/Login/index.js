import React, { Component } from 'react';
import { connect } from 'react-redux';

import './style.css';

// import Constants from '../../Constants';
// import LocalStorage from '../../utils/LocalStorage';
import DomProcess from '../../../utils/DOMProcess';

class Login extends Component {

    LoginTabs = {
        signIn: 'SIGN_IN',
        signUp: 'SIGN_UP',
        reset: 'RESET'
    };

    constructor(props) {
        super(props);
        
        this.state = {
            currentTab: this.LoginTabs.signIn
        };
    }

    titleTabClick = (e) => {

        DomProcess.removeClassFromElem(this.LoginFormTitleTabSignin, 'LoginForm__Title__TabBtn-active');
        DomProcess.removeClassFromElem(this.LoginFormTitleTabSignup, 'LoginForm__Title__TabBtn-active');
        DomProcess.removeClassFromElem(this.LoginFormTitleTabReset, 'LoginForm__Title__TabBtn-active');
        
        DomProcess.removeClassFromElem(this.LoginFormFieldEmail, 'LoginForm__Fields__TextField-hide');
        DomProcess.removeClassFromElem(this.LoginFormFieldPassword, 'LoginForm__Fields__TextField-hide');
        DomProcess.removeClassFromElem(this.LoginFormFieldRepeatPassword, 'LoginForm__Fields__TextField-hide');

        switch(e.target.getAttribute('id')) {

            case 'idLoginFormTitleTabSignin' : {
                this.LoginFormSubmitBtn.textContent = 'Sign in';
                DomProcess.toggleClassElem(e.target, 'LoginForm__Title__TabBtn-active');
                DomProcess.addClassToElem(this.LoginFormFieldRepeatPassword, 'LoginForm__Fields__TextField-hide');
                break;
            }

            case 'idLoginFormTitleTabSignup' : {
                this.LoginFormSubmitBtn.textContent = 'Sign up';
                DomProcess.toggleClassElem(e.target, 'LoginForm__Title__TabBtn-active');
                break;
            }

            case 'LoginFormTitleTabReset' : {
                this.LoginFormSubmitBtn.textContent = 'Reset';
                DomProcess.toggleClassElem(e.target, 'LoginForm__Title__TabBtn-active');
                DomProcess.addClassToElem(this.LoginFormFieldPassword, 'LoginForm__Fields__TextField-hide');
                DomProcess.addClassToElem(this.LoginFormFieldRepeatPassword, 'LoginForm__Fields__TextField-hide');
                break;
            }

            default : return;
        }

    };

    render() {
        return (
            <div className="LoginForm">

                <div className="LoginForm__Title">

                    <button className="LoginForm__Title__TabBtn LoginForm__Title__TabBtn-active"
                            id="idLoginFormTitleTabSignin"
                            ref={(c) => this.LoginFormTitleTabSignin = c}
                            onClick={this.titleTabClick}>
                        Sign in
                    </button>

                    <button className="LoginForm__Title__TabBtn"
                            id="idLoginFormTitleTabSignup"
                            ref={(c) => this.LoginFormTitleTabSignup = c}
                            onClick={this.titleTabClick}>
                        Sign up
                    </button>

                    <button className="LoginForm__Title__TabBtn"
                            id="LoginFormTitleTabReset"
                            ref={(c) => this.LoginFormTitleTabReset = c}
                            onClick={this.titleTabClick}>
                        Register
                    </button>

                </div>
                
                <div className="LoginForm__Fields">

                    <input className="LoginForm__Fields__TextField"
                           type="email"
                           placeholder="Email"
                           ref={(c) => this.LoginFormFieldEmail = c} />

                    <input className="LoginForm__Fields__TextField"
                           type="password"
                           placeholder ="Password"
                           ref={(c) => this.LoginFormFieldPassword = c} />

                    <input className="LoginForm__Fields__TextField LoginForm__Fields__TextField-hide"
                           type="password"
                           placeholder ="Repeat Password"
                           ref={(c) => this.LoginFormFieldRepeatPassword = c} />

                    <button className="LoginForn__SubmitBtn" 
                            ref={(c) => this.LoginFormSubmitBtn = c}>
                        Sign in
                    </button>

                </div>
                
            </div>
        );
    }
}

export default connect(

    state => ({
        AppState: state
    }),

    dispatch => ({

    })

)(Login);

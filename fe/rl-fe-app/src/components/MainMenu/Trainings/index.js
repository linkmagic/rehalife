import React, { Component } from 'react';
import { connect } from 'react-redux';

import './style.css';

import Constants from '../../../Constants';

class Trainings extends Component {

    memoryTopologicalBtnClick = () => {
        this.props.onDisplayContentChange(Constants.DisplayContent.trainingTopologicalMemory);
    };

    render() {
        const { list, current } = this.props.AppState.uiLang;
        let uiTran = list[0];
        for (let i = 0; i < list.length; i++) {
            if (current === list[i].id) {
                uiTran = list[i];
                break;
            }
        }

        return (
            <div className="MainMenu">

                <div className="MainMenu__Column">
                    <div className="MainMenu__Section__Title">{uiTran ? uiTran.mainMenu.trainings.attention.title : ''}</div>
                    <div className="MainMenu__Section__Trainings">
                        <button className="TrainingBtn">
                            {uiTran ? uiTran.mainMenu.trainings.attention.alertness : ''}
                        </button>
                        <button className="TrainingBtn">
                            {uiTran ? uiTran.mainMenu.trainings.attention.reactionBehaviour : ''}
                        </button>
                        <button className="TrainingBtn">
                            {uiTran ? uiTran.mainMenu.trainings.attention.responsiveness : ''}
                        </button>
                        <button className="TrainingBtn">
                            {uiTran ? uiTran.mainMenu.trainings.attention.vigilance : ''}
                        </button>
                        <button className="TrainingBtn">
                            {uiTran ? uiTran.mainMenu.trainings.attention.spatialOperations : ''}
                        </button>
                        <button className="TrainingBtn">
                            {uiTran ? uiTran.mainMenu.trainings.attention.twoDOperations : ''}
                        </button>
                        <button className="TrainingBtn">
                            {uiTran ? uiTran.mainMenu.trainings.attention.threeDOperations : ''}
                        </button>
                        <button className="TrainingBtn">
                            {uiTran ? uiTran.mainMenu.trainings.attention.attentionConcentration : ''}
                        </button>
                        <button className="TrainingBtn">
                            {uiTran ? uiTran.mainMenu.trainings.attention.dividedAttention : ''}
                        </button>
                    </div>
                </div>

                <div className="MainMenu__Column">
                    <div className="MainMenu__Section__Title">
                        {uiTran ? uiTran.mainMenu.trainings.memory.title : ''}
                    </div>
                    <div className="MainMenu__Section__Trainings">
                        <button className="TrainingBtn">
                            {uiTran ? uiTran.mainMenu.trainings.memory.workingMemory : ''}
                        </button>
                        <button className="TrainingBtn" onClick={this.memoryTopologicalBtnClick}>
                            {uiTran ? uiTran.mainMenu.trainings.memory.topologicalMemory : ''}
                        </button>
                        <button className="TrainingBtn">
                            {uiTran ? uiTran.mainMenu.trainings.memory.physiognomicMemory : ''}
                        </button>
                        <button className="TrainingBtn">
                            {uiTran ? uiTran.mainMenu.trainings.memory.memoryWords : ''}
                        </button>
                        <button className="TrainingBtn">
                            {uiTran ? uiTran.mainMenu.trainings.memory.figuralMemory : ''}
                        </button>
                        <button className="TrainingBtn">
                            {uiTran ? uiTran.mainMenu.trainings.memory.verbalMemory : ''}
                        </button>
                    </div>
                </div>

                <div className="MainMenu__Column">
                    <div className="MainMenu__Section__Title">{uiTran ? uiTran.mainMenu.trainings.executiveFunc.title : ''}</div>
                    <div className="MainMenu__Section__Trainings">
                        <button className="TrainingBtn">
                            {uiTran ? uiTran.mainMenu.trainings.executiveFunc.shopping : ''}
                        </button>
                        <button className="TrainingBtn">
                            {uiTran ? uiTran.mainMenu.trainings.executiveFunc.planHoliDay : ''}
                        </button>
                        <button className="TrainingBtn">
                            {uiTran ? uiTran.mainMenu.trainings.executiveFunc.logicalReasoning : ''}
                        </button>
                        <button className="TrainingBtn">
                            {uiTran ? uiTran.mainMenu.trainings.executiveFunc.calculations : ''}
                        </button>
                    </div>
                </div>

                <div className="MainMenu__Column">
                    <div className="MainMenu__Section__Title">{uiTran ? uiTran.mainMenu.trainings.visualField.title : ''}</div>
                    <div className="MainMenu__Section__Trainings">
                        <button className="TrainingBtn">
                            {uiTran ? uiTran.mainMenu.trainings.visualField.saccadicTraining : ''}
                        </button>
                        <button className="TrainingBtn">
                            {uiTran ? uiTran.mainMenu.trainings.visualField.exploration : ''}
                        </button>
                        <button className="TrainingBtn">
                            {uiTran ? uiTran.mainMenu.trainings.visualField.overviewReading : ''}
                        </button>
                    </div>
                </div>

            </div>
        );
    }

}

export default connect(

    state => ({
        AppState: state
    }),

    dispatch => ({
        
        onDisplayContentChange: (action) => {
            dispatch({ type: Constants.ReducerEvent.displayContent, action });
        }

    })

)(Trainings);

import React, { Component } from 'react';
import { connect } from 'react-redux';

import './style.css';

import Constants from '../../Constants';
// import LocalStorage from '../../utils/LocalStorage';
import uiLang from '../../uiLang.json';

import LangSwitcher from '../Header/LangSwitcher';
import UserButton from '../Header/UserButton';
import Login from '../Popups/Login';
import Trainings from '../MainMenu/Trainings';
import Topological from '../Trainings/Memory/Topological';

class App extends Component {

    constructor(props) {
        super(props);
        this.props.onUiLangDownload(uiLang);
    }

    displayContent = () => {
        const { displayContent } = this.props.AppState;

        switch (displayContent.name) {

            case Constants.DisplayContent.home: {
                return <h1>Home</h1>;
            }

            case Constants.DisplayContent.help: {
                return <h1>Help</h1>;
            }

            case Constants.DisplayContent.login: {
                return <Login/>;
            }

            case Constants.DisplayContent.mainMenuTrainings: {
                return <Trainings/>;
            }

            case Constants.DisplayContent.mainMenuScreening: {
                return (<h1>Screening</h1>);
            }

            case Constants.DisplayContent.trainingTopologicalMemory: {
                return <Topological/>;
            }

            default: return '';
        }
    };

    headerHomeBtnClick = () => {
        this.props.onDisplayContentChange(Constants.DisplayContent.home);
    };

    headerTrainingsBtnClick = () => {
        this.props.onDisplayContentChange(Constants.DisplayContent.mainMenuTrainings);
    };

    headerScreeningBtnClick = () => {
        this.props.onDisplayContentChange(Constants.DisplayContent.mainMenuScreening);
    };

    headerHelpBtnClick = () => {
        this.props.onDisplayContentChange(Constants.DisplayContent.help);
    };

    render() {
        const { list, current } = this.props.AppState.uiLang;
        let uiTran = list[0];
        for (let i = 0; i < list.length; i++) {
            if (current === list[i].id) {
                uiTran = list[i];
                break;
            }
        }

        return (
            <div className="App">

                <div className="NavBar">

                    <div className="NavBar__Buttons">

                        <span className="LogoButton">
                            <img src="img/logo__transparent.png" alt=""/>
                            <span>REHAB</span>
                        </span>

                        <span className="InlineIndent"/>

                        <button className="ToolButton" onClick={this.headerHomeBtnClick}>
                            {uiTran ? uiTran.mainMenu.home.title : ''}
                        </button>

                        <button className="ToolButton" onClick={this.headerTrainingsBtnClick}>
                            {uiTran ? uiTran.mainMenu.trainings.title : ''}
                        </button>

                        <button className="ToolButton" onClick={this.headerScreeningBtnClick}>
                            {uiTran ? uiTran.mainMenu.screening.title : ''}
                        </button>

                        <button className="ToolButton" onClick={this.headerHelpBtnClick}>
                            {uiTran ? uiTran.mainMenu.help.title : ''}
                        </button>
                    </div>

                    <LangSwitcher/>
                    <UserButton/>

                </div>

                {this.displayContent()}

            </div>
        );
    }

}

export default connect(

    state => ({
        AppState: state
    }),

    dispatch => ({

        onDisplayContentChange: (action) => {
            dispatch({ type: Constants.ReducerEvent.displayContent, action });
        },

        onUiLangDownload: (action) => {
            dispatch({ type: Constants.ReducerEvent.uiLangDownload, action })
        },

        onUiLangChange: (action) => {
            dispatch({ type: Constants.ReducerEvent.uiLangChange, action })
        }

    })

)(App);

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import './style.css';
import Constants from './Constants';
import LocalStorage from './utils/LocalStorage';
import App from './components/App';
import reducer from './reducers';
import registerServiceWorker from './registerServiceWorker';

const store = createStore(
    reducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

// localStorage initialization
if (!LocalStorage.find(Constants.LocalStorageKey.uiLang)) {
    LocalStorage.setStr(Constants.LocalStorageKey.uiLang, Constants.DefaultValues.uiLangId);
}

ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>,
    document.getElementById('root')
);

registerServiceWorker();
